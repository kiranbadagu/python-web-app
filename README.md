TO RUN DOCKERFILE
CD Python Web App/
sudo docker build --tag my-python-app .

TO CHECK DOCKER IMAGES
sudo docker images

TO RUN DOCKER IMAGE INTO CONTAINER 
sudo docker run -itd --name python-app -p 5000:5000 my-python-app

TO CEHCK DOCKER CONTAINER
sudo docker ps -a

TO STOP/START DOCKER CONTAINER
sudo docker stop [contianer ID]
sudo docker start [container ID]

TO REMOVE DOCKER CONTAINER
sudo docker stop [container ID]
sudo docker rm [container ID]
sudo docker ps -a

TO REMOVE DOCKER IMAGE
sudo docker rmi [image ID]
sudo docker images